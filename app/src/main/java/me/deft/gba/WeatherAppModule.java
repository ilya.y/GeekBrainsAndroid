package me.deft.gba;

import android.content.Context;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

import me.deft.gba.ui.AppActivity;
import me.deft.gba.ui.DetailsFragmentModule;
import me.deft.gba.ui.LoadingScreenView;
import me.deft.gba.ui.SetupFragment;
import me.deft.gba.ui.WeatherMapFragment;
import me.deft.gba.utils.AppSettings;

/**
 * Created by iyakovlev on 03.09.17.
 */


@Module (subcomponents = {
        DetailsFragmentModule.DetailsFragmentSubcomponent.class,
})
public abstract class WeatherAppModule {

    @Singleton
    @Binds
    abstract Context bindAppContext(WeatherApp app);

    @Singleton
    @Provides
    static AppSettings provideAppSettings(Context context) {
        return new AppSettings(context);
    }

    @Singleton
    @Provides
    static Handler provideMainThreadHandler() {
        return new Handler(Looper.getMainLooper());
    }
    @Singleton
    @Provides
    static LocationManager provideLocationManager(Context context) {
        return (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
    }
    @ContributesAndroidInjector
    abstract AppActivity contributeMainActivityInjector();

    @ContributesAndroidInjector
    abstract SetupFragment contributeSetupFragmentInjector();

    @ContributesAndroidInjector
    abstract WeatherMapFragment contributeWeatherMapFragmentInjector();

    @ContributesAndroidInjector
    abstract WeatherUpdateService contributeServiceInjector();

}
