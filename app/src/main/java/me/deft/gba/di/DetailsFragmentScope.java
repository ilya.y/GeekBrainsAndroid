package me.deft.gba.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by iyakovlev on 05.09.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DetailsFragmentScope {
}
