package me.deft.gba;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.deft.gba.ui.DetailsFragmentModule;

/**
 * Created by iyakovlev on 03.09.17.
 */

@Singleton
@Component(modules = {
        WeatherAppModule.class,
        AndroidInjectionModule.class,
        DetailsFragmentModule.class
})
public interface WeatherAppComponent {

    void inject(WeatherApp app);

    @Component.Builder
    interface WeatherAppComponentBuilder {

        WeatherAppComponent build();

        @BindsInstance
        WeatherAppComponentBuilder withWeatherApp(WeatherApp app);
    }

}
