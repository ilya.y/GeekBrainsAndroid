package me.deft.gba;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import me.deft.gba.data.WeatherData;
import me.deft.gba.repository.WeatherRepository;
import me.deft.gba.repository.network.RequestListener;
import me.deft.gba.utils.AppSettings;

/**
 * Created by iyakovlev on 08.10.2017.
 */

public class WeatherUpdateService extends Service {

    private static final String TAG = WeatherUpdateService.class.getSimpleName();
    
    private static final String EXTRA_UPDATE_INTEVAL = "extra_update_interval";
    private static final int UPDATE_INTEVAL_DEFAULT = 1000 * 60 * 5; // 5 min

    @Inject
    public AppSettings mAppSettings;
    @Inject
    public WeatherRepository mRepository;
    @Inject
    public Handler mMainThreadHandler;


    private Handler mServiceHandler;;
    private WeatherUpdateListener mListener;

    public interface WeatherUpdateListener {
        void onWeaterUpdated(WeatherData data);
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        Log.d(TAG, "onCreate: ");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand: ");
        initHandler();

        int delayTime = intent.getIntExtra(EXTRA_UPDATE_INTEVAL, -1);
        if (delayTime == -1)
            delayTime = UPDATE_INTEVAL_DEFAULT;

        postDelayedUpdate(delayTime);

        return START_STICKY;
    }

    private void initHandler() {
        HandlerThread ht = new HandlerThread("Service-Weather-Update");
        ht.start();
        mServiceHandler = new Handler(ht.getLooper());
    }
    private void postDelayedUpdate(final int delayTime) {
        if (mServiceHandler != null) {
            mServiceHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String city = mAppSettings.getLastCityName();
                    Log.d(TAG, "Update weather data for city " + city);
                    mRepository.getWeather(city, new RequestListener<WeatherData>() {
                        @Override
                        public void onResult(final WeatherData result, boolean isRemote) {
                            if (mListener == null || !isRemote)
                                return;

                            mMainThreadHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mListener.onWeaterUpdated(result);
                                }
                            });
                        }

                        @Override
                        public void onError(Exception exception) {

                        }
                    });
                    Log.d(TAG, "run: ");
                    postDelayedUpdate(delayTime);
                }
            }, delayTime);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new WeatherServiceBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        if (mServiceHandler != null)
            mServiceHandler.getLooper().quit();
    }

    public class WeatherServiceBinder extends Binder {

        public void setListener(WeatherUpdateListener listener) {
            mListener = listener;
        }

        public void removeListener() {
            mListener = null;
        }
    }
}
