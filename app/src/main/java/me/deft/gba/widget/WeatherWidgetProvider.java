package me.deft.gba.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import me.deft.gba.R;
import me.deft.gba.data.WeatherType;
import me.deft.gba.repository.local.WeatherContentProvider;
import me.deft.gba.repository.local.WeatherDB;
import me.deft.gba.utils.AppSettings;
import me.deft.gba.utils.WeatherResourceResolver;

/**
 * Created by iyakovlev on 18.10.17.
 */

public class WeatherWidgetProvider extends AppWidgetProvider {

    private static final String TAG = WeatherWidgetProvider.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
        AppSettings settings = new AppSettings(context);
        String cityName = settings.getLastCityName();
        Log.d(TAG, "city: " + cityName);
        if (cityName == null || cityName.isEmpty()) {
            showNoCityText(views);
        } else {
            loadWeather(context, views, cityName);
        }
        appWidgetManager.updateAppWidget(appWidgetIds, views);
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private void showNoCityText(RemoteViews views) {
        views.setViewVisibility(R.id.widget_image_weather, View.GONE);
        views.setViewVisibility(R.id.widget_text_temperature, View.GONE);
        views.setViewVisibility(R.id.widget_text_weather_description, View.GONE);
        views.setViewVisibility(R.id.widget_text_city, View.GONE);
        views.setViewVisibility(R.id.widget_empty_text, View.VISIBLE);
    }

    private void showWeatherViews(RemoteViews views) {
        views.setViewVisibility(R.id.widget_image_weather, View.VISIBLE);
        views.setViewVisibility(R.id.widget_text_temperature, View.VISIBLE);
        views.setViewVisibility(R.id.widget_text_weather_description, View.VISIBLE);
        views.setViewVisibility(R.id.widget_text_city, View.VISIBLE);
        views.setViewVisibility(R.id.widget_empty_text, View.GONE);
    }


    private void loadWeather(Context context, RemoteViews views, String city) {
        Cursor c = context.getContentResolver().query(
                Uri.parse("content://me.deft.gba.provider/weather/"),
                WeatherContentProvider.WEATHER_COLUMNS,
                WeatherDB.COLUMN_CITY_NAME + " = ?",
                new String[] { city},
                null
        );

        if (c.getCount() == 0)
            showNoCityText(views);

        c.moveToFirst();
        final int temp = c.getInt(c.getColumnIndex(WeatherDB.COLUMN_TEMPERATURE));
        final WeatherType type  = WeatherType.valueOf(c.getString(c.getColumnIndex(WeatherDB.COLUMN_WEATHER_TYPE)));
        final String cityName = c.getString(c.getColumnIndex(WeatherDB.COLUMN_CITY_NAME));



        views.setTextViewText(R.id.widget_text_temperature, String.format(context.getResources().getString(R.string.details_tv_temperature),temp));
        views.setTextViewText(R.id.widget_text_city, cityName);
        views.setTextViewText(R.id.widget_text_weather_description, context.getText(WeatherResourceResolver.getDescription(type)));
        views.setImageViewResource(R.id.widget_image_weather,  WeatherResourceResolver.getScrimImage(type));

        showWeatherViews(views);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    @Override
    public void onRestored(Context context, int[] oldWidgetIds, int[] newWidgetIds) {
        super.onRestored(context, oldWidgetIds, newWidgetIds);
    }
}
