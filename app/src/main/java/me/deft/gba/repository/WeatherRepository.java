package me.deft.gba.repository;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.deft.gba.data.ForecastData;
import me.deft.gba.data.WeatherData;
import me.deft.gba.repository.local.WeatherLocalRepository;
import me.deft.gba.repository.network.OpenWeatherRemoteRepository;
import me.deft.gba.repository.network.RequestListener;

/**
 * Created by iyakovlev on 28.09.17.
 */

@Singleton
public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper());

    private WeatherLocalRepository mLocalRepository;
    private OpenWeatherRemoteRepository mRemoteRepository;

    @Inject
    public WeatherRepository(WeatherLocalRepository localRepository, OpenWeatherRemoteRepository remoteRepository) {
        mLocalRepository = localRepository;
        mRemoteRepository = remoteRepository;

    }

    public void getWeather(final String city, final RequestListener<WeatherData> listener) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final WeatherData dataLocal = mLocalRepository.getCurrentWeather(city);
                if (dataLocal != null) {
                    Log.d(TAG, "Found local data");
                    mMainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (listener != null)
                                listener.onResult(dataLocal, false);
                        }
                    });
                }

                try {
                    final WeatherData dataRemote = mRemoteRepository.getCurrentWeather(city);
                    if (dataRemote != null) {
                        mMainThreadHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "Found remote data");
                                if (listener != null)
                                    listener.onResult(dataRemote, true);
                                mLocalRepository.replaceWeatherData(city, dataRemote);
                            }
                        });
                    } else {
                        if (listener != null){
                            mMainThreadHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onError(new IllegalArgumentException("Unknown city"));
                                }
                            });
                        }

                    }
                } catch (final IOException e) {
                    Log.d(TAG, "remote data fetch error");
                    if (listener != null){
                        mMainThreadHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(e);
                            }
                        });
                    }
                }
            }
        });
    }

    public void getForecast(final String city, final RequestListener<List<ForecastData>> listener) {
        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                final List<ForecastData> dataLocal = mLocalRepository.get5DaysForecast(city);
                if (dataLocal != null && dataLocal.size() > 0) {
                    Log.d(TAG, "Found local data");
                    mMainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (listener != null)
                                listener.onResult(dataLocal, false);
                        }
                    });
                }

                try {
                    final List<ForecastData> dataRemote = mRemoteRepository.get5DaysForecast(city);
                    mMainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Found remote data");
                            if (listener != null)
                                listener.onResult(dataRemote, true);
                            mLocalRepository.replaceForecastData(city, dataRemote);
                        }
                    });
                } catch (final IOException e) {
                    Log.d(TAG, "remote data fetch error");
                    if (listener != null){
                        mMainThreadHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(e);
                            }
                        });
                    }
                }
            }
        });
    }
}
