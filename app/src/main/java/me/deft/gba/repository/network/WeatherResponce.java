package me.deft.gba.repository.network;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import me.deft.gba.repository.network.data.Clouds;
import me.deft.gba.repository.network.data.Coord;
import me.deft.gba.repository.network.data.Main;
import me.deft.gba.repository.network.data.Sys;
import me.deft.gba.repository.network.data.Weather;
import me.deft.gba.repository.network.data.Wind;

public class WeatherResponce {

    @SerializedName("base")
    @Expose
    public String base;
    @SerializedName("clouds")
    @Expose
    public Clouds clouds;
    @SerializedName("cod")
    @Expose
    public Integer cod;
    @SerializedName("coord")
    @Expose
    public Coord coord;
    @SerializedName("dt")
    @Expose
    public Integer dt;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("main")
    @Expose
    public Main main;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("sys")
    @Expose
    public Sys sys;
    @SerializedName("visibility")
    @Expose
    public Integer visibility;
    @SerializedName("weather")
    @Expose
    public List<Weather> weather = null;
    @SerializedName("wind")
    @Expose
    public Wind wind;

}