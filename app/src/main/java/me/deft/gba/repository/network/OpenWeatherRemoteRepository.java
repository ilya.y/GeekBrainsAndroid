package me.deft.gba.repository.network;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import me.deft.gba.data.ForecastData;
import me.deft.gba.data.ForecastDataConverter;
import me.deft.gba.data.WeatherData;
import me.deft.gba.data.WeatherDataConverter;
import me.deft.gba.repository.network.data.Forecast;
import me.deft.gba.repository.network.data.WeatherInfo;
import okhttp3.Response;


/**
 * Created by iyakovlev on 17.09.17.
 */

public class OpenWeatherRemoteRepository extends NetworkClient {

    private static String CODE_OK = "200";

    private String mLocale = "en";

    private static final int DAYS = 10;

    private final Gson mGson = new Gson();

    private final WeatherDataConverter mWeatherDataConverter = new WeatherDataConverter();
    private final ForecastDataConverter mForecastDataConverter = new ForecastDataConverter();

    @Inject
    public OpenWeatherRemoteRepository(Context context) {
        mLocale = context.getResources().getConfiguration().locale.getCountry();
    }

    public @Nullable List<ForecastData> get5DaysForecast(String cityName) throws IOException, ClassCastException{
        String url = new RequestBuilder()
                .setLocale(mLocale)
                .setRequestType(RequestBuilder.FORECAST)
                .setUnits(RequestBuilder.METRIC)
                .setQeury(cityName)
                .build();
        Response response = get(url);
        Forecast forecast =  mGson.fromJson(response.body().string(), Forecast.class);
        if (forecast.getCod().equals(CODE_OK)) {
            return mForecastDataConverter.to(forecast);
        }
        return null;
    }

    public @Nullable WeatherData getCurrentWeather(String cityName) throws IOException, ClassCastException {
        String url = new RequestBuilder()
                .setLocale(mLocale)
                .setRequestType(RequestBuilder.WEATHER)
                .setUnits(RequestBuilder.METRIC)
                .setQeury(cityName)
                .build();
        Response response = get(url);
        WeatherInfo info = mGson.fromJson(response.body().string(), WeatherInfo.class);
        if (info.getCod().equals(CODE_OK))
            return mWeatherDataConverter.to(info);
        return null;
    }

    private static class RequestBuilder {

        public static final int FORECAST = 1;
        public static final int WEATHER = 2;
        public static final int METRIC = 1;
        public static final int IMPERIAL = 2;

        private static final String API_KEY = "a59ba9134bb68b6ef92a305aaa2a5d17";
        private static final String BASE_API = "https://api.openweathermap.org/data/2.5/";
        private static final String UNITS_METRIC = "metric";
        private static final String UNITS_IMPERIAL = "imperial";
        private static final String REQUEST_FORECAST = "forecast";
        private static final String REQUEST_WEATHER = "weather";

        private String mUnits = UNITS_METRIC;
        private String mLocale = "en";
        private String mRequestType = REQUEST_FORECAST;
        private String mQeury = "";

        public RequestBuilder setUnits(int units) {
            switch (units) {
                case METRIC: mUnits = UNITS_METRIC; return this;
                case IMPERIAL: mUnits = UNITS_IMPERIAL; return this;
            }

            return this;
        }

        public RequestBuilder setQeury(String qeury) {
            mQeury = qeury;
            return this;
        }

        public RequestBuilder setLocale(String locale) {
            mLocale = locale;
            return this;
        }

        public RequestBuilder setRequestType(int requestType) {
            switch (requestType) {
                case FORECAST: mRequestType = REQUEST_FORECAST; return this;
                case WEATHER: mRequestType = REQUEST_WEATHER; return this;
            }
            return this;
        }

        public final String build() {
            String request = BASE_API + mRequestType + "?units=" + mUnits +
                    "&lang=" + mLocale + "&appid=" + API_KEY + "&q=" + mQeury;
            return request;
        }
    }
}
