
package me.deft.gba.repository.network.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Forecast extends ApiResponse {

    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("list")
    @Expose
    private java.util.List<WeatherInfo> list = null;

    public City getCity() {
        return city;
    }


    public java.util.List<WeatherInfo> getForecasts() {
        return list;
    }


}