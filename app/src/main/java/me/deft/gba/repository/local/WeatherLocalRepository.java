package me.deft.gba.repository.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.deft.gba.data.ForecastData;
import me.deft.gba.data.WeatherData;
import me.deft.gba.data.WeatherType;
import me.deft.gba.repository.network.data.Forecast;
import me.deft.gba.repository.network.data.WeatherInfo;
import okhttp3.Response;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class WeatherLocalRepository {

    private WeatherDB mWeatherDB;

    @Inject
    public WeatherLocalRepository(WeatherDB weatherDB) {
        mWeatherDB = weatherDB;
    }

    public @Nullable List<ForecastData> get5DaysForecast(String cityName) {
       Cursor c = mWeatherDB.getReadableDatabase().rawQuery(
               WeatherDB.SQL_SELECT_FORECAST, new String[]{cityName}
       );

        if (c.getCount() == 0) {
            c.close();
            return null;
        }

       List<ForecastData> forecastData = new LinkedList<>();

       while (c.moveToNext()) {
           ForecastData data = new ForecastData();
           data.setTs(c.getLong(c.getColumnIndex(WeatherDB.COLUMN_DATE)));
           data.setTemp(c.getInt(c.getColumnIndex(WeatherDB.COLUMN_TEMPERATURE)));
           data.setWeatherType(WeatherType.valueOf(c.getString(c.getColumnIndex(WeatherDB.COLUMN_WEATHER_TYPE))));
           forecastData.add(data);
       }

       c.close();

       return forecastData;
    }

    public @Nullable WeatherData getCurrentWeather(String cityName) {
        Cursor c = mWeatherDB.getReadableDatabase().rawQuery(
                WeatherDB.SQL_SELECT_WEATHER, new String[]{cityName}
        );
        if (c.getCount() == 0) {
            c.close();
            return null;
        }

        c.moveToNext();
        WeatherData data = new WeatherData();
        data.setCity(c.getString(c.getColumnIndex(WeatherDB.COLUMN_CITY_NAME)));
        data.setTs(c.getLong(c.getColumnIndex(WeatherDB.COLUMN_DATE)));
        data.setWind(c.getInt(c.getColumnIndex(WeatherDB.COLUMN_WIND)));
        data.setPressure(c.getInt(c.getColumnIndex(WeatherDB.COLUMN_PRESSURE)));
        data.setTemp(c.getInt(c.getColumnIndex(WeatherDB.COLUMN_TEMPERATURE)));
        data.setWeatherType(WeatherType.valueOf(c.getString(c.getColumnIndex(WeatherDB.COLUMN_WEATHER_TYPE))));

        c.close();
        return data;
    }

    public void replaceWeatherData(String city, WeatherData data) {
        SQLiteDatabase db = mWeatherDB.getWritableDatabase();
        db.beginTransaction();

        db.delete(WeatherDB.TABLE_WEATHER, WeatherDB.COLUMN_CITY_NAME + "=?", new String[]{city});
        ContentValues cv = new ContentValues();
        cv.put(WeatherDB.COLUMN_DATE, data.getTs());
        cv.put(WeatherDB.COLUMN_PRESSURE, data.getPressure());
        cv.put(WeatherDB.COLUMN_TEMPERATURE, data.getTemp());
        cv.put(WeatherDB.COLUMN_WEATHER_TYPE, data.getWeatherType().name());
        cv.put(WeatherDB.COLUMN_WIND, data.getWind());
        cv.put(WeatherDB.COLUMN_CITY_NAME, data.getCity());
        db.insert(WeatherDB.TABLE_WEATHER, null, cv);
        db.setTransactionSuccessful();

        db.endTransaction();
    }

    public void replaceForecastData(String city, List<ForecastData> data) {
        SQLiteDatabase db = mWeatherDB.getWritableDatabase();
        db.beginTransaction();

        db.delete(WeatherDB.TABLE_5_DAY_FORECAST, WeatherDB.COLUMN_CITY_NAME + "=?", new String[]{city});

        ContentValues cv = new ContentValues();
        cv.put(WeatherDB.COLUMN_CITY_NAME, city);

        for (ForecastData forecastData : data) {
            cv.put(WeatherDB.COLUMN_WEATHER_TYPE, forecastData.getWeatherType().name());
            cv.put(WeatherDB.COLUMN_TEMPERATURE, forecastData.getTemp());
            cv.put(WeatherDB.COLUMN_DATE, forecastData.getTs());

            db.insert(WeatherDB.TABLE_5_DAY_FORECAST, null, cv);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
