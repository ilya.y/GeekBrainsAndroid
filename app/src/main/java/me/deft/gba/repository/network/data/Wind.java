package me.deft.gba.repository.network.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("deg")
    @Expose
    private Double deg;
    @SerializedName("speed")
    @Expose
    private Double speed;

    public Double getDeg() {
        return deg;
    }

    public void setDeg(Double deg) {
        this.deg = deg;
    }

    public int getSpeed() {
        return speed.intValue();
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

}