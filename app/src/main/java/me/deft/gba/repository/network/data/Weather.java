package me.deft.gba.repository.network.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weather {


    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getId() {
        return id;
    }


}