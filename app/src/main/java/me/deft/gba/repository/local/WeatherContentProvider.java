package me.deft.gba.repository.local;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by iyakovlev on 28.09.17.
 */

public class WeatherContentProvider extends ContentProvider {

    public static final String AUTHORITY = "me.deft.gba.provider";
    private static final UriMatcher sUriMathcher = new UriMatcher(UriMatcher.NO_MATCH);
    public static final String[] WEATHER_COLUMNS = {
        WeatherDB.COLUMN_CITY_NAME,
        WeatherDB.COLUMN_PRESSURE,
        WeatherDB.COLUMN_TEMPERATURE,
        WeatherDB.COLUMN_WEATHER_TYPE,
        WeatherDB.COLUMN_WIND
    };
    public static final String WEATHER_WHERE = WeatherDB.COLUMN_CITY_NAME + "=?";

    public static final String[] FORECAST_COLUMNS = {
            WeatherDB.COLUMN_CITY_NAME,
            WeatherDB.COLUMN_TEMPERATURE,
            WeatherDB.COLUMN_WEATHER_TYPE,
    };
    public static final String FORECAST_WHERE = WEATHER_WHERE;


    static {
        sUriMathcher.addURI(AUTHORITY, "weather", 1);
        sUriMathcher.addURI(AUTHORITY, "forecast", 2);
    }

    private WeatherDB mWeatherDB;

    @Override
    public boolean onCreate() {
        mWeatherDB = new WeatherDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        switch (sUriMathcher.match(uri)) {
            case 1:
                return mWeatherDB.getReadableDatabase().query(
                        WeatherDB.TABLE_WEATHER,
                        strings,
                        s,
                        strings1,
                        null,
                        null,
                        null
                );
            case 2:
                return mWeatherDB.getReadableDatabase().query(
                        WeatherDB.TABLE_5_DAY_FORECAST,
                        strings,
                        s,
                        strings1,
                        null,
                        null,
                        null
                );
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        throw new UnsupportedOperationException("Insert is not supported");
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException("Delete is not supported");
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException("Update is not supported");
    }
}
