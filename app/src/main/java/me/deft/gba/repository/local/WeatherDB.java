package me.deft.gba.repository.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by iyakovlev on 27.09.17.
 */

@Singleton
public class WeatherDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "weather.db";
    private static final int DB_VERSION = 1;

    public static final String TABLE_WEATHER = "weather";
    public static final String TABLE_5_DAY_FORECAST = "forecast";

    public static final String COLUMN_TEMPERATURE = "temp";
    public static final String COLUMN_PRESSURE = "pressure";
    public static final String COLUMN_WIND = "wind";
    public static final String COLUMN_WEATHER_TYPE = "weather";
    public static final String COLUMN_CITY_NAME = "name";
    public static final String COLUMN_DATE = "date_ts";

    private static final String SQL_CREATE_TABLE_WEATHER =
            "CREATE TABLE " +  TABLE_WEATHER + " (" +
                    COLUMN_CITY_NAME + " TEXT UNIQUE NOT NULL," +
                    COLUMN_TEMPERATURE + " INTEGER," +
                    COLUMN_PRESSURE + " INTEGER," +
                    COLUMN_WIND + " INTEGER," +
                    COLUMN_WEATHER_TYPE + " TEXT," +
                    COLUMN_DATE + " INTEGER" +
                    ")";

    private static final String SQL_CREATE_TABLE_FORECAST =
            "CREATE TABLE " +  TABLE_5_DAY_FORECAST + " (" +
                    COLUMN_CITY_NAME + " TEXT NOT NULL," +
                    COLUMN_TEMPERATURE + " INTEGER," +
                    COLUMN_WEATHER_TYPE + " TEXT," +
                    COLUMN_DATE + " INTEGER" +
                    ")";

    private static final String SQL_DROP_TABLE_WEATHER =
            "DROP TABLE " + TABLE_WEATHER + ";";

   private static final String SQL_DROP_TABLE_FORECAST =
            "DROP TABLE " + TABLE_5_DAY_FORECAST + ";";

   public static final String SQL_SELECT_WEATHER =
           "SELECT * FROM " + TABLE_WEATHER + " WHERE " + COLUMN_CITY_NAME + "=?;";

   public static final String SQL_SELECT_FORECAST =
           "SELECT * FROM " + TABLE_5_DAY_FORECAST + " WHERE " + COLUMN_CITY_NAME + "=?;";

   public static final String SQL_DELETE_CITY_WEATHER = "" +
           "DELETE FROM " + TABLE_WEATHER + " WHERE " + COLUMN_CITY_NAME + "=?;";

    public static final String SQL_DELETE_CITY_FORECAST = "" +
            "DELETE FROM " + TABLE_5_DAY_FORECAST + " WHERE " + COLUMN_CITY_NAME + "=?;";


   @Inject
   public WeatherDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_FORECAST);
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_WEATHER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_FORECAST);
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_WEATHER);

        onCreate(sqLiteDatabase);
    }



}
