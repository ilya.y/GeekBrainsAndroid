package me.deft.gba.repository.network;

/**
 * Created by iyakovlev on 28.09.17.
 */

public interface RequestListener<T> {
    void onResult(T result, boolean isRemote);
    void onError(Exception exception);
}
