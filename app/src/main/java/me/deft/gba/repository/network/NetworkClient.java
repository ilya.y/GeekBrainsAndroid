package me.deft.gba.repository.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import me.deft.gba.repository.network.data.ApiResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by iyakovlev on 17.09.17.
 */

public class NetworkClient {

    private static final String TAG = "NetworkClient";
    private final OkHttpClient mOkHttpClient = new OkHttpClient();

    protected Response get(String url) throws IOException{

        Request r = new Request.Builder()
                .url(url)
                .build();
        Log.i(TAG, "Requsting url: " + url);

        return mOkHttpClient.newCall(r).execute();
    }
}
