package me.deft.gba.repository.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by iyakovlev on 26.09.17.
 */

@Singleton
public class CitiesDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "cities.db";
    private static final int DB_VERSION = 1;

    public static final String TABLE_CITIES = "cities";
    public static final String COLUMN_CITY_NAME = "name";
    public static final String COLUMN_ADDED_TS = "created_ts";

    private static final String SQL_CREATE_TABLE_CITIES =
            "CREATE TABLE " +  TABLE_CITIES + " (" +
                    COLUMN_CITY_NAME + " TEXT UNIQUE NOT NULL," +
                    COLUMN_ADDED_TS + " INTEGER"
            + ")";

    private static final String SQL_DROP_TABLE_CITIES =
           "DROP TABLE " + TABLE_CITIES + ";";

    private static final String SQL_SELECT_ALL = "SELECT (" + COLUMN_CITY_NAME + "," + COLUMN_ADDED_TS + ")"
            + " FROM" + TABLE_CITIES + " ORDER BY " + COLUMN_ADDED_TS + " DESC;" ;


    @Inject
    public CitiesDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_CITIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_CITIES);
        onCreate(sqLiteDatabase);
    }

    public void saveCity(String cityName) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CITY_NAME, cityName);
        cv.put(COLUMN_ADDED_TS, System.currentTimeMillis());

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        int res = db.update(TABLE_CITIES, cv, COLUMN_CITY_NAME + "=?", new String[]{cityName});
        if (res == 0) {
            db.insert(TABLE_CITIES, null, cv);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public List<String> getSavedCities() {
        dbw().beginTransaction();
        Cursor c = dbr().rawQuery(SQL_SELECT_ALL, null);

        List<String> cities = new LinkedList<>();
        while (c.moveToNext()) {
            cities.add(c.getString(c.getColumnIndex(COLUMN_CITY_NAME)));
        }
        c.close();

        return cities;
    }

    private SQLiteDatabase dbw() {
        return getWritableDatabase();
    }

    private SQLiteDatabase dbr() {
        return getReadableDatabase();
    }
}
