
package me.deft.gba.repository.network.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {


    @SerializedName("humidity")
    @Expose
    private Integer humidity;
    @SerializedName("pressure")
    @Expose
    private Double pressure;

    @SerializedName("temp")
    @Expose
    private Double temp;

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public int getPressure() {
        return (int) ((pressure - 20) * 0.7500616); // это костыль, т.к. Openwethermap отдает слишком высокое давление всегда: https://openweathermap.desk.com/customer/portal/questions/16803800-wrong-forecast-pressure-for-netherlands-germany
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public int getTemp() {
        return temp.intValue();
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }



}