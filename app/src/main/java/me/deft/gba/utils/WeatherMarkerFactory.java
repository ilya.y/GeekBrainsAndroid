package me.deft.gba.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import me.deft.gba.R;
import me.deft.gba.data.WeatherType;

/**
 * Created by iyakovlev on 05.10.17.
 */

public class WeatherMarkerFactory {

    private Context mContext;

    @Inject
    public WeatherMarkerFactory(Context context) {
        mContext = context;
    }

    public MarkerOptions createForWeather(String city, int temp, WeatherType weatherType) {
        final String markerText = city + ": " + temp + "°C";

        // сначала создаем кисть, которой мы будем рисовать текст
        // и определяем размер рамки для текста
        int textSize = 14; // 14 sp
        float scaledTextSize = mContext.getResources().getDisplayMetrics().density * textSize;
        Rect textRect = new Rect();
        Paint p = new Paint();
        p.setColor(ContextCompat.getColor(mContext, android.R.color.white));
        p.setTextAlign(Paint.Align.LEFT);
        p.setTextSize(scaledTextSize);
        p.getTextBounds(markerText, 0, markerText.length(), textRect);

        // теперь грузим рисунок с иконкой погоды
        int weatherRes = WeatherResourceResolver.getImage(weatherType);
        Bitmap b = BitmapFactory.decodeResource(mContext.getResources(), weatherRes);

        final int margin = 10; // отступы от краев маркера
        final int markerWidth = textRect.width() + margin * 2; // *3 потому что у нас две строки текста - это отступы и междустрочный интервал
        final int markerHeight = textRect.height() + b.getHeight() + margin * 3;
        // создаем битмап с учетом размеров текста и иконки погоды
        Bitmap bitmap = Bitmap.createBitmap(
                mContext.getResources().getDisplayMetrics(),
                markerWidth,
                markerHeight,
                Bitmap.Config.ARGB_8888);

        // создаем холст и заливаем его цветом
        Canvas canvas = new Canvas(bitmap);
        Paint canvasPaint = new Paint();
        canvasPaint.setColor(ContextCompat.getColor(mContext, R.color.colorMarkerBackground));
        final float cornerRadius = 6f;
        canvas.drawRoundRect(new RectF(0,0, markerWidth, markerHeight), cornerRadius, cornerRadius, canvasPaint);
        // рисуем текст и иконку погоды под ним
        float shiftText = margin;
        float shiftIcon = shiftText * 2;
        canvas.drawText(markerText, 10.0f, textRect.height() + shiftText, p);
        canvas.drawBitmap(b, 5.0f + textRect.width() / 2 - b.getWidth() / 2,  textRect.height() + shiftIcon, p);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
        return markerOptions;
    }
}
