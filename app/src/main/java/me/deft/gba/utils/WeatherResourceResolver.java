package me.deft.gba.utils;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import me.deft.gba.R;
import me.deft.gba.data.WeatherType;

/**
 * Created by iyakovlev on 09.09.17.
 */

public class WeatherResourceResolver {



    @Inject
    public WeatherResourceResolver( ) {
    }

    private static final Map<WeatherType, Integer> mTextMap = new HashMap<>();
    private static final Map<WeatherType, Integer> mIconMap = new HashMap<>();
    private static final Map<WeatherType, Integer> mScrimImageMap = new HashMap<>();

    static {
        mTextMap.put(WeatherType.CLEAR, R.string.weather_clear);
        mTextMap.put(WeatherType.CLOUDY, R.string.weather_cloudy);
        mTextMap.put(WeatherType.FOG, R.string.weather_fog);
        mTextMap.put(WeatherType.HAIL, R.string.weather_hail);
        mTextMap.put(WeatherType.RAIN, R.string.weather_rain);
        mTextMap.put(WeatherType.SLEET, R.string.weather_sleet);
        mTextMap.put(WeatherType.SNOW, R.string.weather_snow);
        mTextMap.put(WeatherType.THUNDERSTORM, R.string.weather_thunderstorm);
        mTextMap.put(WeatherType.WIND, R.string.weather_wind);

        mIconMap.put(WeatherType.CLEAR, R.drawable.sunny); //TODO rename
        mIconMap.put(WeatherType.CLOUDY, R.drawable.cloudy);
        mIconMap.put(WeatherType.FOG, R.drawable.fog);
        mIconMap.put(WeatherType.HAIL, R.drawable.hail);
        mIconMap.put(WeatherType.RAIN, R.drawable.rain);
        mIconMap.put(WeatherType.SLEET, R.drawable.sleet);
        mIconMap.put(WeatherType.SNOW, R.drawable.snow);
        mIconMap.put(WeatherType.THUNDERSTORM, R.drawable.thunderstorm);
        mIconMap.put(WeatherType.WIND, R.drawable.wind);

        mScrimImageMap.put(WeatherType.CLEAR, R.drawable.scrim_sunny);
        mScrimImageMap.put(WeatherType.CLOUDY, R.drawable.scrim_cloudy);
        mScrimImageMap.put(WeatherType.FOG, R.drawable.scrim_fog);
        mScrimImageMap.put(WeatherType.RAIN, R.drawable.scrim_rain);
        mScrimImageMap.put(WeatherType.SNOW, R.drawable.scrim_snow);
        mScrimImageMap.put(WeatherType.THUNDERSTORM, R.drawable.scrim_thunder);
        mScrimImageMap.put(WeatherType.WIND, R.drawable.scrim_windy);
    }

    public static @StringRes int getDescription(WeatherType type) {
        Integer resId = mTextMap.get(type);
        if (resId == null)
            throw new IllegalArgumentException("Unknown weather type");
        return resId;
    }

    public static @DrawableRes int getImage(WeatherType type) {
        Integer resId = mIconMap.get(type);
        if (resId == null)
            throw new IllegalArgumentException("Unknown weather type");
        return resId;
    }
    public static @DrawableRes int getScrimImage(WeatherType type) {
        Integer resId = mScrimImageMap.get(type);
        if (resId == null)
            return R.drawable.scrim_sunny; // TODO
        return resId;
    }

}