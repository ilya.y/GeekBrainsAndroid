package me.deft.gba.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import me.deft.gba.data.WeatherData;

/**
 * Created by iyakovlev on 30.08.17.
 */

public class AppSettings {

    public static final int CITY_ID_NOT_FOUND = -1;
    private static final String SETTINGS = "settings";
    private static final String SETTING_LAST_CITY = "setting_last_city_id";
    private static final String SETTING_LAST_CITY_NAME = "setting_last_city_name";
    private static final String SETTING_SHOW_PRESSURE = "setting_show_pressure";
    private static final String SETTING_SHOW_WINDSPEED = "setting_show_windspeed";
    private static final String SETTING_LAST_WINDSPEED = "setting_last_windspeed";
    private static final String SETTING_LAST_PRESSURE = "setting_last_pressure";
    private static final String SETTING_LAST_TEMPERATURE = "setting_last_temp";
    private static final String SETTING_LAST_WEATHER_TYPE = "setting_last_weathertype";


    private SharedPreferences mSharedPreferences;

    @Inject
    public AppSettings(Context context) {
        mSharedPreferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
    }

    public void setLastCityId(int id) {
        mSharedPreferences.edit().putInt(SETTING_LAST_CITY, id).apply();
    }

    public void setShowPressure(boolean showPressure){
        mSharedPreferences.edit().putBoolean(SETTING_SHOW_PRESSURE, showPressure).apply();
    }

    public void setShowWindSpeed(boolean showWindSpeed){
        mSharedPreferences.edit().putBoolean(SETTING_SHOW_WINDSPEED, showWindSpeed).apply();
    }

    public int getLastCityId(){
        return mSharedPreferences.getInt(SETTING_LAST_CITY, CITY_ID_NOT_FOUND);
    }

    public String getLastCityName(){
        return mSharedPreferences.getString(SETTING_LAST_CITY_NAME, "");
    }

    public void setLastCityName(String cityName){
        mSharedPreferences.edit().putString(SETTING_LAST_CITY_NAME, cityName).apply();
    }

    public boolean getShowPressure() {
        return mSharedPreferences.getBoolean(SETTING_SHOW_PRESSURE, true);
    }

    public boolean getShowWindspeed() {
        return mSharedPreferences.getBoolean(SETTING_SHOW_WINDSPEED, true);
    }

    public void testClearSavedCityId() {
        mSharedPreferences.edit().remove(SETTING_LAST_CITY).apply();
    }

    public void testClearSavedCityName() {
        mSharedPreferences.edit().remove(SETTING_LAST_CITY_NAME).apply();
    }
}
