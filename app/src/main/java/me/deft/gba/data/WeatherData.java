package me.deft.gba.data;

/**
 * Created by iyakovlev on 27.09.17.
 */

public class WeatherData extends ForecastData {
    private int mPressure;
    private int mWind;
    private String mCity;

    public int getPressure() {
        return mPressure;
    }

    public void setPressure(int pressure) {
        mPressure = pressure;
    }

    public int getWind() {
        return mWind;
    }

    public void setWind(int wind) {
        mWind = wind;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    @Override
    public String toString() {
        return "Погода в : " + mCity + "\n" +
                "Давление, мм. рт. ст.: " + mPressure +
                "\nСкорость ветра, м/c: " + mWind;
    }
}
