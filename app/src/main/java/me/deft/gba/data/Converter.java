package me.deft.gba.data;

/**
 * Created by iyakovlev on 27.09.17.
 */

public interface Converter<F,T> {

     T to(F from);
}
