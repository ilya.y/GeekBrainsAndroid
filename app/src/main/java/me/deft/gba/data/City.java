package me.deft.gba.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by iyakovlev on 31.08.17.
 */

public class City {

    private String mName;

    public City(String name ) {
        mName = name;
    }

    public String getName() {
        return mName;
    }



    @Override
    public String toString() {
        return mName;
    }


}
