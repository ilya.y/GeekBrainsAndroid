package me.deft.gba.data;

import me.deft.gba.repository.network.data.WeatherInfo;

/**
 * Created by iyakovlev on 27.09.17.
 */

public class WeatherDataConverter implements Converter<WeatherInfo, WeatherData> {

    @Override
    public WeatherData to(WeatherInfo from) {

        WeatherData data = new WeatherData();
        data.setPressure(from.getMain().getPressure());
        data.setWind(from.getWind().getSpeed());
        data.setTemp(from.getMain().getTemp());
        data.setTs(from.getDt());
        data.setWeatherType(WeatherType.forCode(from.getWeather().get(0).getId()));
        data.setCity(from.getName());

        return data;
    }
}
