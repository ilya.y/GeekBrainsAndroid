package me.deft.gba.data;

/**
 * Created by iyakovlev on 07.09.17.
 */

public class ForecastConfig {

    private boolean mShowWindspeed = true;
    private boolean mShowPressure = true;

    public ForecastConfig setShowWindspeed(boolean showWindspeed) {
        mShowWindspeed = showWindspeed;
        return this;
    }

    public ForecastConfig setShowPressure(boolean showPressure) {
        mShowPressure = showPressure;
        return this;
    }

    public boolean isShowWindspeed() {
        return mShowWindspeed;
    }

    public boolean isShowPressure() {
        return mShowPressure;
    }
}
