package me.deft.gba.data;

/**
 * Created by iyakovlev on 09.09.17.
 */

public enum WeatherType {

    RAIN,
    SNOW,
    SLEET,  // дождь со снегом
    HAIL,   // Град
    THUNDERSTORM,
    CLOUDY,
    FOG,
    WIND,
    CLEAR;

    public static WeatherType forCode(int code) {
        if (isClear(code))
            return CLEAR;
        if (isThunderStorm(code))
            return THUNDERSTORM;
        if (isRain(code))
            return RAIN;
        if (isSnow(code))
            return SNOW;
        if (isSleet(code))
            return SLEET;
        if (isHail(code))
            return HAIL;
        if (isCloudy(code))
            return CLOUDY;
        if (isFog(code))
            return FOG;
        if (isWind(code))
            return WIND;


        return getGenericWeatherType(code);
    }

    private static WeatherType getGenericWeatherType(int code) {
        if (code == 800)
            return CLEAR;

        int n = code / 100;

        switch (n) {
            case 2: return THUNDERSTORM;
            case 3: return RAIN;
            case 5: return RAIN;
            case 6: return SNOW;
            case 7: return FOG;
            case 8: return CLOUDY;
            default:return CLEAR;
        }
    }

    private static boolean isThunderStorm(int code) {
        return code < 300;
    }

    private static boolean isRain(int code) {
        return code / 100 == 5;
    }

    private static boolean isSnow(int code) {
        switch (code) {
            case 600: return true;
            case 601: return true;
            case 602: return true;
            default: return false;
        }
    }

    private static boolean isSleet(int code) {
        return code > 602 && code < 622;
    }

    private static boolean isHail(int code) {
        return code == 906;
    }

    private static boolean isCloudy(int code) {
        return code / 100 == 8;
    }

    private static boolean isFog(int code) {
        return code == 741 || code == 721 || code == 701 || code == 711;
    }

    private static boolean isWind(int code) {
        return code == 905;
    }

    private static boolean isClear(int code) {
        return code == 800;
    }




}
