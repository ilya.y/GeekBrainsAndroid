package me.deft.gba.data;

/**
 * Created by iyakovlev on 27.09.17.
 */

public class ForecastData {
    private int mTemp;
    private long mTs;
    private WeatherType mWeatherType;

    public WeatherType getWeatherType() {
        return mWeatherType;
    }

    public void setWeatherType(WeatherType weatherType) {
        mWeatherType = weatherType;
    }

    public int getTemp() {
        return mTemp;
    }

    public void setTemp(int temp) {
        mTemp = temp;
    }

    public long getTs() {
        return mTs;
    }

    public void setTs(long ts) {
        mTs = ts;
    }
}
