package me.deft.gba.data;

import java.util.LinkedList;
import java.util.List;

import me.deft.gba.repository.network.data.Forecast;
import me.deft.gba.repository.network.data.WeatherInfo;

/**
 * Created by iyakovlev on 27.09.17.
 */

public class ForecastDataConverter implements Converter<Forecast, List<ForecastData>> {

    @Override
    public List<ForecastData> to(Forecast from) {
        List<ForecastData> data = new LinkedList<>();

        for (WeatherInfo info : from.getForecasts()) {
            ForecastData forecastData = new ForecastData();
            forecastData.setWeatherType(WeatherType.forCode(info.getWeather().get(0).getId()));
            forecastData.setTemp(info.getMain().getTemp());
            forecastData.setTs(info.getDt());
            data.add(forecastData);
        }

        return data;
    }
}
