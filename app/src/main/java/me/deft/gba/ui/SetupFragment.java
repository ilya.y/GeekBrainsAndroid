package me.deft.gba.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import me.deft.gba.R;
import me.deft.gba.utils.AppSettings;
import me.deft.gba.utils.LocationUtils;

/**
 * Created by iyakovlev on 02.10.2017.
 */

public class SetupFragment extends LoadableFragment {

    private static final String TAG = SetupFragment.class.getSimpleName();
    private static final int REQUEST_PERMISSION_COARSE_LOCATION = 1;

    @Inject
    public AppSettings mAppSettings;

    private Button mNextButton;
    private EditText mCityEditText;

    @Inject
    public LocationManager mLocationManager;
    private Handler mLocationCallbackThreadHandler;

    @Inject
    public Handler mMainThreadHandler;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
           setCity(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.d(TAG, "onStatusChanged: " + s);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.d(TAG, "onProviderEnabled: " + s);
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.d(TAG, "onProviderDisabled: " + s);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View v = inflater.inflate(R.layout.fragment_setup, container, false);
        if (!(getActivity() instanceof LoadingScreenView))
            throw new IllegalStateException("Parent activity must implement LoadingScreenView interface");
        mNextButton = v.findViewById(R.id.setup_button_proceed);
        mCityEditText = v.findViewById(R.id.setup_city_edit_text);

        mNextButton.setEnabled(false);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNextButton.setEnabled(false);
                mAppSettings.setLastCityName(mCityEditText.getText().toString());
                ((AppActivity)getActivity()).openMainScreen(mCityEditText.getText().toString());
            }
        });
        mCityEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    mNextButton.setEnabled(true);
                } else {
                    mNextButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        HandlerThread thread = new HandlerThread("Location-Listener-thread");
        thread.start();
        mLocationCallbackThreadHandler = new Handler(thread.getLooper());

        setRetainInstance(true);
        return v;
    }


    @SuppressWarnings("MissingPermission")
    private Location checkLastLocation() {
        if (LocationUtils.checkFineLocationPermission(getContext())) {
            Location fineLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (fineLocation != null)
                return fineLocation;

            return mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        // если нам надо запросить пермишен, то ничего не делаем; иначе сразу начинаем юзать локацию
        Location location = checkLastLocation();
        if (location != null) {
            setCity(location);
        } else {
            if (!requestPermissionIfNeeded()) {
                requestLocation();
            }
        }
    }

    /**
     * returns true in case we have requested permissions
     * @return true if permission was requested; false otherwise
     */
    private boolean requestPermissionIfNeeded() {

        int permissionCheckFine = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheckFine != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSION_COARSE_LOCATION
                );
                return true;
            }

        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(mLocationListener);
        mLocationCallbackThreadHandler.getLooper().quit();
    }

    @Override
    @SuppressWarnings("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_COARSE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Location location = checkLastLocation();
                if (location != null) {
                    setCity(location);
                } else {
                    requestLocation();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);
    }

    private void setCity(final Location location) {
        if (location == null || !mCityEditText.getText().toString().isEmpty())
            return;

        Geocoder geocoder = new Geocoder(getContext());
        try {
            List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 15);
            //Костыль, т.к. это locality приходит далеко не всегда
            for (final Address address : addressList) {
                if (address.getLocality() != null) {
                    mMainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCityEditText.setText(address.getLocality());
                        }
                    });
                    return;
                }
            }

        } catch (IOException e) {
            Log.d(TAG, "unable to fetch geocoded data");
        }
    }

    // нам не надо постоянно следить за локацией, запрашиваем её только один раз при настройке экрана
    @SuppressWarnings("MissingPermission")
    private void requestLocation() {
        String provider = null;
        if (mLocationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
            // пытаемся запросить последнюю локацию, если была
            mLocationManager.requestSingleUpdate(LocationManager.PASSIVE_PROVIDER, mLocationListener, mLocationCallbackThreadHandler.getLooper());
        if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
        }
        if (provider == null)
            return;

        mLocationManager.requestSingleUpdate(provider, mLocationListener, mLocationCallbackThreadHandler.getLooper());
    }
}
