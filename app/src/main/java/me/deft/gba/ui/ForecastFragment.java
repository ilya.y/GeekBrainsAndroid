package me.deft.gba.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import me.deft.gba.R;
import me.deft.gba.data.City;
import me.deft.gba.data.ForecastData;
import me.deft.gba.repository.WeatherRepository;
import me.deft.gba.repository.local.WeatherDB;
import me.deft.gba.repository.network.NetworkClient;
import me.deft.gba.repository.network.OpenWeatherRemoteRepository;
import me.deft.gba.repository.network.RequestListener;
import me.deft.gba.utils.AppSettings;

/**
 * Created by iyakovlev on 17.09.17.
 */

public class ForecastFragment extends Fragment {

    @Inject
    public ForecastAdapter mForecastAdapter;
    @Inject
    public WeatherRepository mWeatherRepository;
    @Inject
    public AppSettings mAppSettings;
    @Inject
    public WeatherDB mWeatherDB;

    private RecyclerView mForecastRecyclerView;
    private City mCurrentCity;

    public static ForecastFragment getFragment(String city) {
        ForecastFragment forecastFragment = new ForecastFragment();
        forecastFragment.mCurrentCity = new City(city);
        return forecastFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        setRetainInstance(true);
        View fragmentView = inflater.inflate(R.layout.fragment_forecast, container, false);
        mForecastRecyclerView = fragmentView.findViewById(R.id.details2_recyclerview_forecast);
        mForecastRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mForecastRecyclerView.setAdapter(mForecastAdapter);

        return fragmentView;
    }

    public void refreshForecast(City city) {

        if (city == null)
            throw new IllegalStateException("Empty city");
        mWeatherRepository.getForecast(city.getName(), new RequestListener<List<ForecastData>>() {
            @Override
            public void onResult(List<ForecastData> result, boolean isRemote) {
                mForecastAdapter.setForecasts(result);
                mForecastAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Exception exception) {
              //  Snackbar.make(getView(), R.string.error_loading_data, BaseTransientBottomBar.LENGTH_INDEFINITE).show();
            }
        });
    }
}
