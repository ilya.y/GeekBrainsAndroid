package me.deft.gba.ui;

import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;

import me.deft.gba.R;
import me.deft.gba.data.ForecastData;
import me.deft.gba.utils.WeatherResourceResolver;

/**
 * Created by iyakovlev on 09.09.17.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    WeatherResourceResolver mWeatherDescriptionResolver;

    DateFormat mDateFormat;

    List<ForecastData> mWeatherInfos;

    public ForecastAdapter(WeatherResourceResolver resolver, DateFormat format) {
        mDateFormat = format;
        mWeatherDescriptionResolver = resolver;
    }

    public void setForecasts(List<ForecastData> forecasts) {
        mWeatherInfos = forecasts;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_recyclerview_weather, parent, false);
        return new ForecastViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        ForecastData forecast = mWeatherInfos.get(position);
        holder.setDay(mDateFormat.format(new java.util.Date(forecast.getTs() * 1000)));
        holder.setTemperature(forecast.getTemp());
        holder.setWeatherIcon(mWeatherDescriptionResolver.getImage(forecast.getWeatherType()));
    }

    @Override
    public int getItemCount() {
        return mWeatherInfos == null ? 0 : mWeatherInfos.size();
    }

    public static class ForecastViewHolder extends RecyclerView.ViewHolder {

        TextView mTemperature;
        TextView mDay;
        ImageView mWeatherIcon;

        final String mTemperaturePattern;

        public ForecastViewHolder(View itemView) {
            super(itemView);
            mTemperature = itemView.findViewById(R.id.rv_item_textview_temperature);
            mDay = itemView.findViewById(R.id.rv_item_textview_day);
            mWeatherIcon = itemView.findViewById(R.id.rv_item_image_weather);

            mTemperaturePattern = itemView.getContext().getResources().getString(R.string.details_tv_temperature);
        }

        public void setTemperature(int temperature) {
            mTemperature.setText(String.format(mTemperaturePattern, temperature));
        }

        public void setDay(String day) {
            mDay.setText(day);
        }

        public void setWeatherIcon(@DrawableRes int res) {
            mWeatherIcon.setImageResource(res);
        }
    }
}
