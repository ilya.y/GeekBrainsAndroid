package me.deft.gba.ui;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;
import me.deft.gba.di.DetailsFragmentScope;
import me.deft.gba.utils.WeatherResourceResolver;

/**
 * Created by iyakovlev on 05.09.17.
 */

@Module (subcomponents = DetailsFragmentModule.DetailsFragmentSubcomponent.class)
public abstract class DetailsFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(DetailsFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
        bindDetailsFragmentInjectionFactory(DetailsFragmentSubcomponent.Builder builder);

    @Provides
    static ForecastAdapter provideForecastAdapter(WeatherResourceResolver resolver, DateFormat format) {
        return new ForecastAdapter(resolver, format);
    }

    @Provides
    static DateFormat provideSimpleDateFormat(Context context) {
        return new SimpleDateFormat("EEE, d, H:mm", context.getResources().getConfiguration().locale);
    }



    @ContributesAndroidInjector
    abstract ForecastFragment contributeForecstFragmentInjector();


    @Subcomponent
        @DetailsFragmentScope
    public interface DetailsFragmentSubcomponent extends AndroidInjector<DetailsFragment>{

        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<DetailsFragment> {


        }

        void inject(ForecastAdapter adapter);
    }


}
