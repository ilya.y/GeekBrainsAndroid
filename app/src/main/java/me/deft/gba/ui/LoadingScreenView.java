package me.deft.gba.ui;

/**
 * Created by iyakovlev on 03.10.2017.
 */


public interface LoadingScreenView {
    void showLoadingScreen();
    void hideLoadingScreen();
    boolean isLoading();

}
