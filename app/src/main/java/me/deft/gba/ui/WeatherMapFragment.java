package me.deft.gba.ui;

import android.Manifest;
import android.app.ActionBar;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import me.deft.gba.data.WeatherData;
import me.deft.gba.repository.WeatherRepository;
import me.deft.gba.repository.network.RequestListener;
import me.deft.gba.utils.LocationUtils;
import me.deft.gba.utils.WeatherMarkerFactory;
import me.deft.gba.utils.WeatherResourceResolver;

/**
 * Created by iyakovlev on 04.10.17.
 */

public class WeatherMapFragment extends SupportMapFragment implements OnMapReadyCallback, LocationListener{
    private static final String TAG = WeatherMapFragment.class.getSimpleName();

    private static final int REQUEST_PERMISSION_COARSE_LOCATION = 1;
    private static final int REQUEST_PERMISSION_FINE_LOCATION = 2;

    @Inject
    public LocationManager mLocationManager;
    @Inject
    public WeatherRepository mWeatherRepository;
    @Inject
    public WeatherMarkerFactory mMarkerFactory;
    @Inject
    public Handler mMainThreadHandler;

    private HandlerThread mLocationCallbackHandlerThread;
    private GoogleMap mGoogleMap;

    public static WeatherMapFragment newInstance() {
        Bundle args = new Bundle();
        WeatherMapFragment fragment = new WeatherMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        AndroidSupportInjection.inject(this);
        setRetainInstance(true);
        mLocationCallbackHandlerThread = new HandlerThread("Weather-Map-thread");
        mLocationCallbackHandlerThread.start();
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getMapAsync(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(this);
        mLocationCallbackHandlerThread.getLooper().quit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                drawWeatherMarker(latLng);
            }
        });
        Location location = checkLastLocation();
            if (location != null) {
                final LatLng myPos = (new LatLng(location.getLatitude(), location.getLongitude()));
                drawWeatherMarker(myPos);
        } else {
            if (!requestPermissionIfNeeded()) {
                requestLocation();
            }
        }
    }

    @SuppressWarnings("MissingPermission")
    private Location checkLastLocation() {
        if (LocationUtils.checkFineLocationPermission(getContext())) {
            Location fineLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (fineLocation != null)
                return fineLocation;

            return mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        return null;
    }

    private boolean requestPermissionIfNeeded() {

        if (!LocationUtils.checkFineLocationPermission(getContext())) {
            if (LocationUtils.needToRequestPermissions()) {
                getActivity().requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSION_COARSE_LOCATION
                );
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @SuppressWarnings("MissingPermission")
    private void requestLocation() {
        String provider = null;
        if (mLocationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
            // пытаемся запросить последнюю локацию, если была
            mLocationManager.requestSingleUpdate(LocationManager.PASSIVE_PROVIDER, this, mLocationCallbackHandlerThread.getLooper());
        if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = LocationManager.GPS_PROVIDER;
        }
        if (provider == null)
            return;

        mLocationManager.requestSingleUpdate(provider, this, mLocationCallbackHandlerThread.getLooper());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mGoogleMap != null) {
            final LatLng myPos = (new LatLng(location.getLatitude(), location.getLongitude()));
            drawWeatherMarker(myPos);

        } else {
            // если по каким-то причинам мы получили локацию до того, как подгрузилсь карта
            // то мы просто еще раз реквестим локцию
            requestLocation();
        }
    }

    private void drawWeatherMarker(final LatLng location) {
        Geocoder geocoder = new Geocoder(getContext());
        String tLocation = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(location.latitude, location.longitude, 12);
            for(Address a : addresses) {
                if (a.getLocality() != null) {
                    tLocation = a.getLocality();
                    break;
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "Unable to decode location");
        }
        final String locationName = tLocation;

        if (!locationName.isEmpty()) {
            mWeatherRepository.getWeather(locationName, new RequestListener<WeatherData>() {
                @Override
                public void onResult(final WeatherData result, boolean isRemote) {
                    mMainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mGoogleMap.clear();
                            CameraUpdate pointToMe = CameraUpdateFactory.newLatLng(location);
                            CameraUpdate zoomUpdate = CameraUpdateFactory.newLatLngZoom(location, 5.0f);
                            MarkerOptions m = mMarkerFactory.createForWeather(result.getCity(), result.getTemp(), result.getWeatherType());
                            m.position(location);
                            mGoogleMap.animateCamera(pointToMe);
                            mGoogleMap.addMarker(m);
                        }
                    });
                }

                @Override
                public void onError(Exception exception) {

                }
            });
        }


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
