package me.deft.gba.ui;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.deft.gba.R;

/**
 * Created by iyakovlev on 11.09.17.
 */

public abstract class SelectableAdapter<VH extends SelectableAdapter.SelectableViewHolder>
        extends RecyclerView.Adapter<VH> {

    private ItemSelectionListener mSelectionListener;
    private View mSelectedView;

    private int mCurrentSelection;

    interface ItemSelectionListener {
        void onItemSelected(int itemAdapterPosition);
    }

    public void setSelectionListener(ItemSelectionListener listener) {
        mSelectionListener = listener;
    }

    public void removeSelectionListener() {
        mSelectionListener = null;
    }

    @Override
    public void onBindViewHolder(final VH holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedView != null)
                    removeSelectionForView(mSelectedView);

                mSelectedView = view;
                mCurrentSelection = holder.getAdapterPosition();
                setSelectionForView(mSelectedView);
                if (mSelectionListener != null)
                    mSelectionListener.onItemSelected(mCurrentSelection);
                if (holder.hasOnClickListener())
                    holder.callOnClick();
            }
        });
    }

    @Override
    public void onViewRecycled(VH holder) {
        super.onViewRecycled(holder);
        removeSelectionForView(holder.itemView);
    }

    private void removeSelectionForView(View view) {
        view.setBackgroundColor(Color.TRANSPARENT);
    }

    private void setSelectionForView(View view) {
        view.setBackgroundColor(getSelectionColor());
    }

    abstract int getSelectionColor();



    public static class SelectableViewHolder extends RecyclerView.ViewHolder {

        private View.OnClickListener mAdditionalOnClickListener;

        public SelectableViewHolder(View itemView) {
            super(itemView);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            mAdditionalOnClickListener = listener;
        }

        public boolean hasOnClickListener() {
            return mAdditionalOnClickListener != null;
        }

        public void callOnClick() {
            mAdditionalOnClickListener.onClick(itemView);
        }


    }
}
