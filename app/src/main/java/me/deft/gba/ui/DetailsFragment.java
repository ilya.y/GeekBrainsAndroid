package me.deft.gba.ui;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.transition.Fade;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telecom.ConnectionService;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import me.deft.gba.R;
import me.deft.gba.WeatherUpdateService;
import me.deft.gba.data.City;
import me.deft.gba.data.WeatherData;
import me.deft.gba.data.WeatherType;
import me.deft.gba.repository.WeatherRepository;
import me.deft.gba.repository.network.RequestListener;
import me.deft.gba.utils.AppSettings;
import me.deft.gba.utils.ImageUtils;
import me.deft.gba.utils.WeatherResourceResolver;
import me.deft.gba.widget.WeatherWidgetProvider;

/**
 * Created by iyakovlev on 14.09.17.
 */

public class DetailsFragment extends Fragment implements WeatherUpdateService.WeatherUpdateListener {

    private static final String TAG = "DetailsFragment";
    private static final String EXTRA_CITY = "extra_city";

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (componentName.getClassName().equals(WeatherUpdateService.class.getName())) {
                if (iBinder != null) {
                    mWeatherServiceBinder = (WeatherUpdateService.WeatherServiceBinder) iBinder;
                    mWeatherServiceBinder.setListener(DetailsFragment.this);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            if (mWeatherServiceBinder != null)
                mWeatherServiceBinder.removeListener();
        }
    };

    private static final int REQUEST_CODE_SHARE_WEATHER = 0x1100;

    @Inject
    public AppSettings mAppSettings;
    @Inject
    public WeatherRepository mWeatherRepository;

    private City mCurrentCity;

    private TextView mTemperatureTextView;
    private TextView mWindSpeedTextView;
    private TextView mWeatherTextView;
    private TextView mPressureTextView;

    private Toolbar mToolbar;
    private ImageView mCollapsingToolbarImage;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private AppBarLayout mAppBarLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mDataLoadErrorView;
    private View mLoadingView;
    private View mFragmentContainer;
    private ProgressBar mLoadingProgressBar;
    private Button mReloadDataButton;

    private WeatherUpdateService.WeatherServiceBinder mWeatherServiceBinder;

    private @DrawableRes int mScrimImageResource;

    private WeatherData mLastWeatherData;

    public static DetailsFragment newInstance(@Nullable String city) {

        Bundle args = new Bundle();
        if (city != null)
            args.putString(EXTRA_CITY, city);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);

        setRetainInstance(true);
        setHasOptionsMenu(true);

        if (mCurrentCity == null) {
            String cityName = getArguments().getString(EXTRA_CITY);
            if (cityName == null)
                throw new IllegalStateException("Empty city");

            mCurrentCity = new City(cityName);

        }

        final View fragmentView = inflater.inflate(R.layout.fragment_details, container, false);
        mWindSpeedTextView = fragmentView.findViewById(R.id.details2_textview_windspeed);
        mTemperatureTextView = fragmentView.findViewById(R.id.details_textview_temperature);
        mPressureTextView = fragmentView.findViewById(R.id.details2_textview_pressure);
        mWeatherTextView = fragmentView.findViewById(R.id.details_textview_weather);
        mAppBarLayout = fragmentView.findViewById(R.id.details2_appbar_layout);
        mToolbar = fragmentView.findViewById(R.id.details2_toolbar);
        mCollapsingToolbarImage = fragmentView.findViewById(R.id.details_collapsing_toolbar_image);
        mCollapsingToolbarLayout = fragmentView.findViewById(R.id.details_collpasing_toolbar);
        mSwipeRefreshLayout = fragmentView.findViewById(R.id.details_fragment_swipe_refresh_layout);
        mDataLoadErrorView = fragmentView.findViewById(R.id.details_state_error);
        mLoadingView = fragmentView.findViewById(R.id.details_state_loading);
        mLoadingProgressBar = fragmentView.findViewById(R.id.details_loading_progressbar);
        mReloadDataButton = fragmentView.findViewById(R.id.details_state_error_reload);
        mFragmentContainer = fragmentView.findViewById(R.id.forecast_details);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadWeatherData(mCurrentCity.getName());
            }
        });

        mReloadDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadWeatherData(mCurrentCity.getName());
            }
        });

        initToolbar();

        return fragmentView;
    }


    @Override
    public void onStart() {
        super.onStart();
        loadWeatherData(mCurrentCity.getName());

        Intent intent = new Intent(getContext(), WeatherUpdateService.class);
        getContext().bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    public void onStop() {
        super.onStop();
        getContext().unbindService(mServiceConnection);
    }

    private void setScrimImageResource(int resId) {
        int w = getActivity().getWindow().getDecorView().getWidth();
        int h = getActivity().getWindow().getDecorView().getHeight();
        if (w == 0 || h == 0)
            return;

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), resId, o);
        o.inSampleSize = ImageUtils.calculateInSampleSize(o, w, h);
        o.inJustDecodeBounds = false;
        Bitmap weatherImage = BitmapFactory.decodeResource(getResources(),resId, o);
        mCollapsingToolbarImage.setImageBitmap(weatherImage);

    }

    private void loadWeatherData(final String forCity) {
        if (mLastWeatherData == null)
            showLoadingScreen();

        mWeatherRepository.getWeather(forCity, new RequestListener<WeatherData>() {
            @Override
            public void onResult(WeatherData result, boolean isRemote) {
                if (isRemote && result == null && mLastWeatherData == null) {
                    showErrorScreen();
                    return;
                }
                showWeatherSсreen();
                updateScreenData(result);
                sendWidgetUpdateIntent();
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(Exception exception) {
                if (mLastWeatherData == null) {
                    showErrorScreen();
                } else {
                    Snackbar.make(getView(), R.string.error_loading_data, BaseTransientBottomBar.LENGTH_LONG).show();

                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void updateScreenData(WeatherData data) {
        WeatherType type = data.getWeatherType();

        if (data == null)
            return;

        mLastWeatherData = data;

        mScrimImageResource = WeatherResourceResolver.getScrimImage(type);
        setScrimImageResource(mScrimImageResource);
        mPressureTextView.setText(String.format(getResources().getString(R.string.details_tv_pressure), data.getPressure()));
        mWindSpeedTextView.setText(String.format(getResources().getString(R.string.details_tv_wind_speed), data.getWind()));
        mTemperatureTextView.setText(String.format(getResources().getString(R.string.details_tv_temperature), data.getTemp()));
        mWeatherTextView.setText(getResources().getText(WeatherResourceResolver.getDescription(type)));
        mAppSettings.setLastCityName(data.getCity());

        updateChildFragmentWeather();

        setTitle(data.getCity());
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setNestedFragment();
    }

    private boolean checkSharingAvailable(Intent intent) {
        return getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
                .size() > 0;
    }


    private Intent getSharingIntent() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_TEXT, mLastWeatherData.toString());
        i.setType("text/plain");
        return i;
    }


    public void setTitle(String title) {
        if (mCollapsingToolbarLayout != null) {
            mCollapsingToolbarLayout.setTitle(title);
        } else {
            mToolbar.setTitle(title);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.details_toolbar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details_toolbar_share:
                return shareWeather();
            case R.id.details_toolbar_change_city:
                return openCityDialog();
            case R.id.details_toolbar_weather_map:
                openWeatherMap(); return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openWeatherMap() {
        ((AppActivity)getActivity()).openWeatherMap();
    }

    private boolean shareWeather() {
        Intent intent = getSharingIntent();
        if (checkSharingAvailable(intent)) {
            startActivityForResult(intent, REQUEST_CODE_SHARE_WEATHER);
        } else {
            Toast.makeText(getContext(), getString(R.string.sharing_error), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private boolean openCityDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle);
        View view = getLayoutInflater().inflate(R.layout.dialog_change_city, null, false);
        final EditText editText = view.findViewById(R.id.dialog_edit_text);
        builder.setView(view)
                .setTitle(R.string.change_city)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (checkNetworkAvailable()) {
                            String text = editText.getText().toString();
                            mCurrentCity = new City(text);
                            loadWeatherData(text);
                           // mAppSettings.setLastCityName(text);
                        } else {
                            Snackbar.make(getView(), R.string.error_loading_data, BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    }
                }).show();
        return true;
    }

    private void initToolbar() {
        setTitle(mCurrentCity.getName());
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);

    }

    private void updateChildFragmentWeather() {
        Fragment f = getChildFragmentManager().findFragmentById(R.id.details_framgnet_container);
        if (f != null && f instanceof ForecastFragment) {
            ((ForecastFragment)f).refreshForecast(mCurrentCity);
        }
    }

    private void setNestedFragment() {
        Fragment f = getChildFragmentManager().findFragmentById(R.id.details_framgnet_container);
        if (f == null) {
            f = ForecastFragment.getFragment(mCurrentCity.getName());
            getChildFragmentManager().beginTransaction()
                    .add(R.id.details_framgnet_container, f)
                    .commit();
        }
    }

    @Override
    public void onWeaterUpdated(WeatherData data) {
        updateScreenData(data);
        sendWidgetUpdateIntent();
        Log.d(TAG, "onWeaterUpdated: ");
    }

    private void sendWidgetUpdateIntent() {
        ComponentName componentName = new ComponentName(getContext().getPackageName(), WeatherWidgetProvider.class.getCanonicalName()   );
        int[] widgetIds = AppWidgetManager.getInstance(getContext()).getAppWidgetIds(componentName);
        if (widgetIds.length > 0) {
            Intent updateIntent = new Intent(getContext(), WeatherWidgetProvider.class);
            updateIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
            updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
            getContext().sendBroadcast(updateIntent);
        }
    }

    private boolean checkNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;

    }

    private void showWeatherSсreen() {
        mLoadingView.setVisibility(View.GONE);
        mDataLoadErrorView.setVisibility(View.GONE);
        if (mAppBarLayout != null) {
            mAppBarLayout.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.VISIBLE);
            mFragmentContainer.setVisibility(View.VISIBLE);

        }
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    private void showErrorScreen() {
        mLoadingView.setVisibility(View.GONE);
        mDataLoadErrorView.setVisibility(View.VISIBLE);
        if (mAppBarLayout != null) {
            mAppBarLayout.setVisibility(View.GONE);
        } else {
            mToolbar.setVisibility(View.GONE);
            mFragmentContainer.setVisibility(View.GONE);

        }
        mSwipeRefreshLayout.setVisibility(View.GONE);
    }

    private void showLoadingScreen() {
        mLoadingView.setVisibility(View.VISIBLE);
        mDataLoadErrorView.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        if (mAppBarLayout != null) {
            mAppBarLayout.setVisibility(View.GONE);
        } else {
            mToolbar.setVisibility(View.GONE);
            mFragmentContainer.setVisibility(View.GONE);
        }
    }


}
