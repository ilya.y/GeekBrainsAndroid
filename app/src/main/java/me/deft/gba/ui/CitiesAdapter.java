package me.deft.gba.ui;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import me.deft.gba.R;
import me.deft.gba.data.City;

/**
 * Created by iyakovlev on 11.09.17.
 */

public class CitiesAdapter extends SelectableAdapter<CitiesAdapter.CityViewHolder> {

    List<City> mCityList;
    int mSelectionColor = Color.TRANSPARENT;

    @Inject
    public CitiesAdapter() {
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyclerview_city, parent, false);

        return new CityViewHolder(v);
    }

    public void setCityList(List<City> cityList) {
        mCityList = cityList;
    }

    public void setSelectionColor(int selectionColor) {
        mSelectionColor = selectionColor;
    }

    @Override
    public int getItemCount() {
        return mCityList == null ? 0 : mCityList.size();
    }

    @Override
    int getSelectionColor() {
        return mSelectionColor;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.setCityName(mCityList.get(position).getName());
    }

    public static class CityViewHolder extends SelectableAdapter.SelectableViewHolder {

        private final TextView mCityTextView;

        public CityViewHolder(View itemView) {
            super(itemView);
            mCityTextView = itemView.findViewById(R.id.item_textviw_cityname);
        }

        public void setCityName(String name) {
            mCityTextView.setText(name);
        }
    }
}
