package me.deft.gba.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import me.deft.gba.R;
import me.deft.gba.WeatherUpdateService;
import me.deft.gba.utils.AppSettings;

/**
 * Created by iyakovlev on 14.09.17.
 */


public class AppActivity extends AppCompatActivity implements LoadingScreenView {

    @Inject
    public AppSettings mAppSettings;

    private ContentLoadingProgressBar mProgressBar;
    private View mRootView;

    private Intent mStartServiceIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.screen_progress_bar);
        mRootView = findViewById(R.id.fragment_container);
        mProgressBar.show();

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

        if (mAppSettings.getLastCityName().isEmpty()) {
            if (f == null || !(f instanceof SetupFragment)) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new SetupFragment())
                        .commit();
            }
        } else {
            if (f == null || !(f instanceof DetailsFragment)) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, DetailsFragment.newInstance(mAppSettings.getLastCityName()))
                        .commit();
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mStartServiceIntent = new Intent(this, WeatherUpdateService.class);
        startService(mStartServiceIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mStartServiceIntent != null)
            stopService(mStartServiceIntent);
    }

    public void openMainScreen(String city) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, DetailsFragment.newInstance(city))
                .commit();
    }

    public void openWeatherMap() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, WeatherMapFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showLoadingScreen() {
        mRootView.setVisibility(View.GONE);
        mProgressBar.hide();
    }

    @Override
    public void hideLoadingScreen() {
        mRootView.setVisibility(View.VISIBLE);
        mProgressBar.show();
    }

    @Override
    public boolean isLoading() {
        return mProgressBar.isShown();
    }
}


