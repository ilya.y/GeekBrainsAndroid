package me.deft.gba.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by iyakovlev on 03.10.2017.
 */

public abstract class LoadableFragment extends Fragment {

    private LoadingScreenView mLoadingScreenView;

    protected boolean isLoading() {
        return mLoadingScreenView.isLoading();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!(getActivity() instanceof LoadingScreenView))
            throw new IllegalStateException("Parent activity must implement LoadingScreenView");

        mLoadingScreenView = (LoadingScreenView)getActivity();
    }

    public void showLoading() {
        mLoadingScreenView.showLoadingScreen();
    }

    public void hideLoading() {
        mLoadingScreenView.hideLoadingScreen();
    }
}
